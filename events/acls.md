import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

# def get_lat_lon(city, state):
#     url = "https://api.openweathermap.org/data/2.5/weather?q={city name},{state code},{country code}&appid={API key}"
#     headers = {

#     }


# def get_weather_data(city, state):
#     # geocoding_url = "https://api.openweathermap.org/data/2.5/weather?q={city name},{state code},{country code}&appid={API key}"
#     geocoding_url = "https://api.openweathermap.org/data/2.5/weather"

#     headers = {
#         "Authorization": OPEN_WEATHER_API_KEY,
#     }
#     params = {
#         "q": f"{city} {state}",
#         "appid": OPEN_WEATHER_API_KEY,
#         "limit": 1,
#     }
#     res = requests.get(geocoding_url, params=params, headers=headers)
#     weather_dict = res.json()
#     # get lat lon
#     # get_lat_lon = weather_dict["lat"], weather_dict["lon"]
#     lon = weather_dict["lon"]
#     lat = weather_dict["lat"]

#     current_weather_url = "https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}"
#     params_too = {
#         "lat": lat,
#         "lon": lon,
#     }


def get_weather_data(city, state):
    # get coords from city, state
    geocoding_url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "q": f"{city},{state},USA",
        "appid": OPEN_WEATHER_API_KEY,
    }
    res = requests.get(geocoding_url, params=params)
    coords_dict = res.json()
    print(coords_dict)
    # get lat lon
    lat = coords_dict["coord"]["lat"]
    lon = coords_dict["coord"]["lon"]
    # get temp description based on coords
    current_weather_url = "https://api.openweathermap.org/data/2.5/weather"
    params_too = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    req = requests.get(current_weather_url, params=params_too)
    weather_dict = req.json()
    return {
        "temp": weather_dict["main"]["temp"],
        "description": weather_dict["weather"][0]["description"],
    }


# coords_dict = {
#     "coord": {"lon": 35.3048, "lat": 32.7066},
#     "weather": [
#         {
#             "id": 800,
#             "main": "Clear",
#             "description": "clear sky",
#             "icon": "01n",
#         }
#     ],
#     "base": "stations",
#     "main": {
#         "temp": 70.39,
#         "feels_like": 70.81,
#         "temp_min": 70.39,
#         "temp_max": 70.39,
#         "pressure": 1005,
#         "humidity": 78,
#         "sea_level": 1005,
#         "grnd_level": 959,
#     },
#     "visibility": 10000,
#     "wind": {"speed": 2.89, "deg": 271, "gust": 3.27},
#     "clouds": {"all": 0},
#     "dt": 1660857488,
#     "sys": {
#         "type": 2,
#         "id": 2006970,
#         "country": "IL",
#         "sunrise": 1660878295,
#         "sunset": 1660926021,
#     },
#     "timezone": 10800,
#     "id": 294098,
#     "name": "Nazareth",
#     "cod": 200,
# }


# def get_weather_data(city, state):
#     url = "https://api.openweathermap.org/data/2.5/weather"
#     params = {
#         "q": f"{city}, {state}, USA",
#         "appid": OPEN_WEATHER_API_KEY,
#         "units": "imperial",
#     }
#     res = requests.get(url, params=params)
#     weather_dict = res.json()
#     return {
#         "temp": weather_dict["main"]["temp"],
#         "description": weather_dict["weather"][0]["description"],
#     }

# weather_dict = {
#     "coord": {"lon": 35.3048, "lat": 32.7066},
#     "weather": [
#         {
#             "id": 800,
#             "main": "Clear",
#             "description": "clear sky",
#             "icon": "01n",
#         }
#     ],
#     "base": "stations",
#     "main": {
#         "temp": 70.39,
#         "feels_like": 70.81,
#         "temp_min": 70.39,
#         "temp_max": 70.39,
#         "pressure": 1005,
#         "humidity": 78,
#         "sea_level": 1005,
#         "grnd_level": 959,
#     },
#     "visibility": 10000,
#     "wind": {"speed": 2.89, "deg": 271, "gust": 3.27},
#     "clouds": {"all": 0},
#     "dt": 1660857488,
#     "sys": {
#         "type": 2,
#         "id": 2006970,
#         "country": "IL",
#         "sunrise": 1660878295,
#         "sunset": 1660926021,
#     },
#     "timezone": 10800,
#     "id": 294098,
#     "name": "Nazareth",
#     "cod": 200,
# }


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {
        "Authorization": PEXELS_API_KEY,
    }

    params = {"query": f"{city} {state}", "page": 1, "per_page": 1}
    res = requests.get(url, params=params, headers=headers)
    pexel_dict = res.json()
    # anti-corrupting
    picture_url = pexel_dict["photos"][0]["src"]["original"]
    return {"picture_url": picture_url}
