import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_weather_data(city, state):
    # get coords from city, state
    geocoding_url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "q": f"{city},{state},USA",
        "appid": OPEN_WEATHER_API_KEY,
    }
    res = requests.get(geocoding_url, params=params)
    coords_dict = res.json()
    print(coords_dict)
    # get lat lon
    lat = coords_dict["coord"]["lat"]
    lon = coords_dict["coord"]["lon"]
    # get temp description based on coords
    current_weather_url = "https://api.openweathermap.org/data/2.5/weather"
    params_too = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    req = requests.get(current_weather_url, params=params_too)
    weather_dict = req.json()
    return {
        "temp": weather_dict["main"]["temp"],
        "description": weather_dict["weather"][0]["description"],
    }

def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {
        "Authorization": PEXELS_API_KEY,
    }

    params = {"query": f"{city} {state}", "page": 1, "per_page": 1}
    res = requests.get(url, params=params, headers=headers)
    pexel_dict = res.json()
    # anti-corrupting
    picture_url = pexel_dict["photos"][0]["src"]["original"]
    return {"picture_url": picture_url}



# def get_weather_data(city, state):
#     url = "https://api.openweathermap.org/data/2.5/weather"
#     params = {
#         "q": f"{city}, {state}, USA",
#         "appid": OPEN_WEATHER_API_KEY,
#         "units": "imperial",
#     }
#     res = requests.get(url, params=params)
#     weather_dict = res.json()
#     return {
#         "temp": weather_dict["main"]["temp"],
#         "description": weather_dict["weather"][0]["description"],
#     }



