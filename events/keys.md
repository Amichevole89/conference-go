
<!-- query required by authorization -->
<!-- minimum required! -->
url + '?query=Washington DC'
https://api.pexels.com/v1/search?query=Washington+DC
<!-- using additional parameters -->
url + '?query=Washington DC&page=1*per_page=1' 
https://api.pexels.com/v1/search?query=Washington+DC&page=1+per_page=1
<!-- dictionary instead of inputting string into res= results.get() -->
params = {
    'query': 'Washington DC',
    'page': 1,
    'per_page': 1
}

res = requests.get(url,params=params, headers=headers)

import requests

url = 'https://api.pexels.com/v1/search'
<!-- headers used to allow authorization -->
headers = {
   'Authorization': PEXELS_API_KEY,
}
<!-- using string input to add to URL in res = results.get() -->
res = requests.get(url + '?query=Washington DC&page=1&per_page=1', headers=headers)

res.status_code
pexel_dict = res.json()
pexel_dict['total_results']
pexel_dict['page']
pexel_dict['photos']
len(pexel_dict['photos'])
sorted.(pexel_dict.keys())
pexel_dict['photos'][0]
<!-- to get deep within a list of dictionaries -->
<!-- get into pexel_dict value with key ['photos'] value -->
<!-- key [0] gets value in first dictionary in photos list -->
<!-- key ['srs'] gets value in 'srs' within [0] -->
<!-- ['original'] gets value stored in key 'original' -->
pexel_dict['photos'][0]['src']['original]

content ={
  "name": "George R. Brown Convention Center",
  "city": "Houston",
  "room_count": 36,
  "state": "TX"
}

picture_url_dict = get_photo(content['city'], content['state'])

content.update(picture_url_dict)