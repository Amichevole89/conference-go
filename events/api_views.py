import json
from django.http import JsonResponse
from .models import Conference, Location
from django.views.decorators.http import require_http_methods
from .models import State
from events.acls import get_photo, get_weather_data

# from presentations.api_views import PresentationDetailEncoder

# from .models import State

from common.json import ModelEncoder


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, obj):
        return {"state": obj.state.abbreviation}


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    data = {"locations": Location.objects.all()}
    if request.method == "GET":
        return JsonResponse(data, encoder=LocationListEncoder, safe=False)
    else:
        content = json.loads(request.body)
        # get_photo
        picture_url_dict = get_photo(content["city"], content["state"])
        content.update(picture_url_dict)
        # get the state object and put it in the content dict
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        location = Location.objects.create(**content)
        return JsonResponse(
            location, encoder=LocationDetailEncoder, safe=False
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, pk):
    data = Location.objects.get(id=pk)
    if request.method == "GET":
        return JsonResponse(data, encoder=LocationDetailEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        Location.objects.filter(id=pk).update(**content)
        data = Location.objects.get(id=pk)
        return JsonResponse(data, encoder=LocationDetailEncoder, safe=False)


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "starts",
        "ends",
        "description",
        "created",
        "updated",
        "max_presentations",
        "max_attendees",
        "location",
        # "presentation_titles",
    ]
    encoders = {
        "location": LocationListEncoder(),
        # "presentation_titles": PresentationDetailEncoder(),
    }

    # def get_extra_data(self, get_weather_data):
    #     if not get_weather_data:
    #         return {"weather": "null"}
    #     return {"weather": get_weather_data}


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    data = {"conferences": Conference.objects.all()}
    if request.method == "GET":
        return JsonResponse(
            data,
            encoder=ConferenceListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        conference = Conference.objects.create(**content)

        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, pk):
    if request.method == "GET":
        # weather = get_weather_data(
        #     Conference.objects.get(id=pk).location.city,
        #     Conference.objects.get(id=pk).location.state,
        # )
        conference = Conference.objects.get(id=pk)
        weather = get_weather_data(
            conference.location.city,
            conference.location.state,
        )
        return JsonResponse(
            {"conference": conference, "weather": weather},
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        try:
            content = json.loads(request.body)
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        Conference.objects.filter(id=pk).update(**content)
        conference = Conference.objects.get(id=pk)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
