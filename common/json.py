from json import JSONEncoder
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import QuerySet
from json import JSONEncoder

# import datetime
# class DateEncoder(JSONEncoder):
#     def default(self, o):
#         if isinstance(o, datetime.datetime):
#             return o.isoformat()
#         return super().default(o)
#
# class ModelEncoder(JSONEncoder):
#
# class ModelEncoder(DjangoJSONEncoder):
#     def default(self, obj):
#         if isinstance(obj, self.model):
#             return (
#                 {}.values({getattr(obj, property)})
#                 for property in self.properties
#             )
#         else:
#             return super().default(obj)


class QuerySetEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, QuerySet):
            return list(obj)
        else:
            return super().default(obj)


class ModelEncoder(QuerySetEncoder, DjangoJSONEncoder):
    encoders = {}

    def default(self, obj):
        if isinstance(obj, self.model):
            storage = {}
            if hasattr(obj, "get_api_url"):
                storage["href"] = obj.get_api_url()
            for property in self.properties:
                value = getattr(obj, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                storage[property] = value
            storage.update(self.get_extra_data(obj))
            return storage
        else:
            return super().default(obj)

    def get_extra_data(self, obj):
        return {}
